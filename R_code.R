#--------------------------------------------------------------
# EMBL-EBI Networks and Pathways May 2018 v.0.3
# Matt Rogon Centre for Biomolecular Network Analysis EMBL
# rogon@embl.de
# 
# Tutorial
# R-processing script has been divided into the following sections:
# -	Section 1: preprocessing the data for protein-protein network construction – we will not be spending much time on this, just run and forget
# -	Section 2: mining String for interactions and integrating all datasets using the String combined score
# -	Section 3: annotation with BiomaRt
# -	Section 4: merging of data
# -	Section 5: Creation of an iGraph network, cleanup and saving
# -	QC
# -	Section 6: quick overview of String, and ClusterProfiler enrichment capabilities
# -	Section 7: R and Cytoscape as an integrated environment for network analysis
#
#
# This script is provided under the 
# Creative Commons - Attribution 4.0 international license
# you are free to Share and Adapt und the following conditions
# https://creativecommons.org/licenses/by/4.0/
#
#--------------------------------------------------------------

# set your path
setwd("/Volumes/Work/01. Teaching/CBNA Courses/EBI Networks and Pathways/EBI Networks and Pathways 2018/02. Friday/Proteomic_data")

# load libraries
library(dplyr)
library(plyr)
library(STRINGdb)
library(reshape2)

# source("http://bioconductor.org/biocLite.R") 
# biocLite()
# biocLite("BiocUpgrade")
# biocLite("STRINGdb")
# biocLite("clusterProfiler")
# biocLite("pathview")
# biocLite("org.Hs.eg.db")
# biocLite("biomaRt")
# biocLite("igraph")
# install.packages("dplyr", "plyr", "reshape2", "devtools")
# devtools::install_github(c("GuangchuangYu/DOSE", "GuangchuangYu/clusterProfiler", "GuangchuangYu/ReactomePA"))

#vignette("clusterProfiler", package="clusterProfiler")



#--------------------------------------------------------------
# Section 1
#--------------------------------------------------------------
# Load Datasets
#--------------------------------------------------------------

# NOTE: Instead of using the provided PI file, we'll mine interactions
# from the latest release of STRING
#
# NOTE: subsequent data loading steps are explicit on purpose, but
# can be avoided by either using the standard merge by two columns such as:
# merge(x, y, by=c("id1","id2"))
# or indirectly by using iGraph undirected network and merging

# Load and process individual datasets

# Biological Process
dataset_BP <- read.table("BP_reduced_data.txt", quote="\"", comment.char="", stringsAsFactors=FALSE)

# rename columns
# explicit again, you can aso use as 
# colnames(dataset_BP) <- c("id1","id2","weight_bp") # or
# dataset_BP <- rename(dataset_BP, c("V1" = "id1", "V2" = "id2", "V3" = "weight_bp"))
names(dataset_BP)[names(dataset_BP) == 'V1'] <- 'id1'
names(dataset_BP)[names(dataset_BP) == 'V2'] <- 'id2'
names(dataset_BP)[names(dataset_BP) == 'V3'] <- 'weight_bp'

# merge id columns with "(pp)" as delimiter into interaction ( use \t for tab separator)
dataset_BP$Interaction <- paste(dataset_BP$id1, dataset_BP$id2, sep="(pp)")
# create a flipped version prior to merging
dataset_BP$Interaction_flip <- paste(dataset_BP$id2, dataset_BP$id1, sep="(pp)")
# extract Interaction and scores
dataset_BP_1 <- dplyr::select(dataset_BP, Interaction, weight_bp)
dataset_BP_2 <- dplyr::select(dataset_BP, Interaction_flip, weight_bp)

#rename Interaction_flip to Interaction
names(dataset_BP_2)[names(dataset_BP_2) == 'Interaction_flip'] <- 'Interaction'

# put all together
dataset_BP_fin <- rbind(dataset_BP_1, dataset_BP_2)
#row.names(dataset_BP_fin)<-NULL

#--------------------------------------------------------------
# repeat for the remaining datasets
#--------------------------------------------------------------
#CODA
dataset_CODA <- read.table("CODA80_reduced_data.txt", quote="\"", comment.char="", stringsAsFactors=FALSE)
names(dataset_CODA)[names(dataset_CODA) == 'V1'] <- 'id1'
names(dataset_CODA)[names(dataset_CODA) == 'V2'] <- 'id2'
names(dataset_CODA)[names(dataset_CODA) == 'V3'] <- 'weight_coda'
dataset_CODA$Interaction <- paste(dataset_CODA$id1, dataset_CODA$id2, sep="(pp)")
dataset_CODA$Interaction_flip <- paste(dataset_CODA$id2, dataset_CODA$id1, sep="(pp)")
dataset_CODA_1 <- dplyr::select(dataset_CODA, Interaction, weight_coda)
dataset_CODA_2 <- dplyr::select(dataset_CODA, Interaction_flip, weight_coda)
names(dataset_CODA_2)[names(dataset_CODA_2) == 'Interaction_flip'] <- 'Interaction'
dataset_CODA_fin <- rbind(dataset_CODA_1, dataset_CODA_2)

#--------------------------------------------------------------
#HIPPO
dataset_HIPPO <- read.table("HIPPO_reduced_data.txt", quote="\"", comment.char="", stringsAsFactors=FALSE)
names(dataset_HIPPO)[names(dataset_HIPPO) == 'V1'] <- 'id1'
names(dataset_HIPPO)[names(dataset_HIPPO) == 'V2'] <- 'id2'
names(dataset_HIPPO)[names(dataset_HIPPO) == 'V3'] <- 'weight_hippo'
dataset_HIPPO$Interaction <- paste(dataset_HIPPO$id1, dataset_HIPPO$id2, sep="(pp)")
dataset_HIPPO$Interaction_flip <- paste(dataset_HIPPO$id2, dataset_HIPPO$id1, sep="(pp)")
dataset_HIPPO_1 <- dplyr::select(dataset_HIPPO, Interaction, weight_hippo)
dataset_HIPPO_2 <- dplyr::select(dataset_HIPPO, Interaction_flip, weight_hippo)
names(dataset_HIPPO_2)[names(dataset_HIPPO_2) == 'Interaction_flip'] <- 'Interaction'
dataset_HIPPO_fin <- rbind(dataset_HIPPO_1, dataset_HIPPO_2)

#--------------------------------------------------------------
#TM
dataset_TM <- read.table("TM_reduced_data.txt", quote="\"", comment.char="", stringsAsFactors=FALSE)
names(dataset_TM)[names(dataset_TM) == 'V1'] <- 'id1'
names(dataset_TM)[names(dataset_TM) == 'V2'] <- 'id2'
names(dataset_TM)[names(dataset_TM) == 'V3'] <- 'weight_tm'
dataset_TM$Interaction <- paste(dataset_TM$id1, dataset_TM$id2, sep="(pp)")
dataset_TM$Interaction_flip <- paste(dataset_TM$id2, dataset_TM$id1, sep="(pp)")
dataset_TM_1 <- dplyr::select(dataset_TM, Interaction, weight_tm)
dataset_TM_2 <- dplyr::select(dataset_TM, Interaction_flip, weight_tm)
names(dataset_TM_2)[names(dataset_TM_2) == 'Interaction_flip'] <- 'Interaction'
dataset_TM_fin <- rbind(dataset_TM_1, dataset_TM_2)

# you can now remove unused frames
rm(list = c("dataset_BP_1", "dataset_BP_2", "dataset_CODA_1", "dataset_CODA_2", "dataset_HIPPO_1", "dataset_HIPPO_2", "dataset_PI_1", "dataset_PI_2", "dataset_TM_1", "dataset_TM_2"))

# Intermediate output frames with (pp) are edge lists for Cytoscape

#--------------------------------------------------------------
# Section 2
#--------------------------------------------------------------
# STRING.
#--------------------------------------------------------------
# load the simplified list of genes for the course (1 column with id's)
GeneList <- read.table("selected_genes.txt", quote="\"", comment.char="", stringsAsFactors=FALSE)
GeneList <- unique(GeneList)

#--------------------------------------------------------------
# setup the STRINGdb environment
#--------------------------------------------------------------
string_db <- STRINGdb$new( version="10", species=9606, score_threshold=0, input_directory="" )

# map identifiers to string id's (automatic format detection) - notice that these are 
# ENSEMBL protein ID's concatenated with species id
main_string_mapping = string_db$map(GeneList, "V1", removeUnmappedRows = TRUE )
# We couldn't map to SRTRING 0% of your identifiers = 100% success

# extract interactions from String
main_string_interactions <- string_db$get_interactions(main_string_mapping$STRING_id)

# backup
dataset_string <- main_string_interactions

# Downloaded a total of 1.088 interactions
# Look at the data frame, we will recalculate the combined score in section 4
#
# String score confidence intervals
# 0.15 low
# 0.4 medium
# 0.7 high
# 0.9 very high
# filtered combined score of 400 (medium confidence) or more contains 642 interactions
table(main_string_interactions$combined_score >= 400)
main_string_interactions_400 <- subset(main_string_interactions, combined_score >= 400)
net_400 <- main_string_interactions_400

# get neighbors of a vector of string id's
neighbors <- string_db$get_neighbors(main_string_mapping$STRING_id)

#------------------------------------------------------------------------------------
# trim species taxid from the STRING datasets
#------------------------------------------------------------------------------------
#strip / trim species ID
dataset_string$from <- gsub("9606.", "", dataset_string$from, fixed=TRUE)
dataset_string$to <- gsub("9606.", "", dataset_string$to, fixed=TRUE)



#--------------------------------------------------------------
# Section 3
#------------------------------------------------------------------------------------
# Annotation with BioMart - we need entrez id's for enrichment with ClusterProfiler
# we also need ensembl gene id's for merging, this we already have in string mapping
#------------------------------------------------------------------------------------
library("biomaRt")

# for reproducibility, here is how you can access an older Mart
# ensembl81=useMart("ENSEMBL_MART_ENSEMBL", host="jul2015.archive.ensembl.org/biomart/martservice/", dataset="hsapiens_gene_ensembl")
# ensembl84=useMart("ENSEMBL_MART_ENSEMBL", host="mar2016.archive.ensembl.org/biomart/martservice/", dataset="hsapiens_gene_ensembl")
# ensembl85=useMart("ENSEMBL_MART_ENSEMBL", host="jul2016.archive.ensembl.org/biomart/martservice/", dataset="hsapiens_gene_ensembl")
# and so on...
# check this site for links http://www.ensembl.org/info/website/archives/index.html

#latest Ensembl GRCh38.p5 assembly is (as of May 2017) version 88, you can access it via the direct:
ensembl = useMart("ensembl")
ensembl = useDataset("hsapiens_gene_ensembl", mart=ensembl)

# ??biomaRt 
datasets_biomart <- listDatasets(ensembl)
attributes_biomart <- listAttributes(ensembl)

# note: you can always list the available attributes 
# attributes <- listAttributes(ensembl) 
# we can add e.g. "ensembl_peptide_id", but it wouldn't be useful here, as it
# multiplies the dataset (single gene id maps to multiple protein id's)
#
# string mapper (main_string_mapping) already contains gene to protein annotation

results= getBM(attributes = c("hgnc_symbol","entrezgene", "ensembl_gene_id"),
               filters = c(filters = "ensembl_gene_id"),
               values = main_string_mapping$V1,
               mart = ensembl)


#------------------------------------------------------------------------------------
# to merge all data we need to map it to a common identifier - most datasets use
# ensembl gene id, the only one without it is our new STRING dataset
# 
# here we annotate STRING dataset from ensembl peptide to gene id
# this information is stored in (main_string_mapping) we've created earlier
#------------------------------------------------------------------------------------
# backup
mapper <- main_string_mapping
#  contains ensembl gene id and peptide id - use to map the frame
# strip species ID

# trim species from ensembl protein id
mapper$STRING_id <- gsub("9606.", "", mapper$STRING_id, fixed=TRUE)

# annotate columns
dataset_string <- left_join(dataset_string, mapper, by=c("from"="STRING_id"))
names(dataset_string)[names(dataset_string) == 'V1'] <- 'id1'

dataset_string <- left_join(dataset_string, mapper, by=c("to"="STRING_id"))
names(dataset_string)[names(dataset_string) == 'V1'] <- 'id2'

# since we're merging without iGraph - we need to take care of directionality
# A(pp)B = B(pp)A
# create interaction edge list
dataset_string$Interaction <- paste(dataset_string$id1, dataset_string$id2, sep="(pp)")

# bi-directional
dataset_string$Interaction_flip <- paste(dataset_string$id2, dataset_string$id1, sep="(pp)")
dataset_string_1 <- dplyr::select(dataset_string, Interaction, combined_score)
dataset_string_2 <- dplyr::select(dataset_string, Interaction_flip, combined_score)
names(dataset_string_2)[names(dataset_string_2) == 'Interaction_flip'] <- 'Interaction'
dataset_string_fin <- rbind(dataset_string_1, dataset_string_2)
names(dataset_string_fin)[names(dataset_string_fin) == 'combined_score'] <- 'weight_string'
dataset_string_fin <- unique(dataset_string_fin)


#---------------------------------------------------------------------------------
#---------------------------------------------------------------------------------
# Exercise BioMart
# Create a copy of results dataframe and modify the code above to annotate with description
# and Uniprot/Swissprot
# put that id description into the database query (results) under filters
# if you don't feel like you can do this in R, take the list and use the BioMart web
#---------------------------------------------------------------------------------



#------------------------------------------------------------------------------------
# Section 4
#------------------------------------------------------------------------------------
# MERGE ALL DATA and calculate combined score
#------------------------------------------------------------------------------------
# you can try the classic one by one: merge <- merge(coelution,prediction, by=c("Interaction"), all = TRUE)
# or as shown earlier without creating the edge list (pp) by two columns 
# merge(x, y, by=c("id1","id2"))

# or the faster multiple dataframe reduce and merge
merged_data <- Reduce(function(x, y) merge(x, y, all=TRUE), list(dataset_BP_fin, dataset_CODA_fin, dataset_HIPPO_fin, dataset_string_fin, dataset_TM_fin))

# split the (pp) interaction column
merged_data$id1 <- sapply(strsplit(as.character(merged_data$Interaction), "\\(pp)"), "[[", 1)
merged_data$id2 <- sapply(strsplit(as.character(merged_data$Interaction), "\\(pp)"), "[[", 2)

# backup
merged_data_rescaled <- merged_data

# scale weights to 0-1
library(reshape)
# set NA's to 0
merged_data_rescaled[is.na(merged_data_rescaled)] <- 0

merged_data_rescaled$weight_bp <- rescaler(merged_data_rescaled$weight_bp, "range")
merged_data_rescaled$weight_coda <- rescaler(merged_data_rescaled$weight_coda, "range")
merged_data_rescaled$weight_hippo <- rescaler(merged_data_rescaled$weight_hippo, "range")
merged_data_rescaled$weight_tm <- rescaler(merged_data_rescaled$weight_tm, "range")

# string is scaled 0-1000, thus divide by 1000
merged_data_rescaled$weight_string <- merged_data_rescaled$weight_string/1000

# alternative scaling method with dplyr:
# merged_test <- merged_data_rescaled %>% 
# mutate_each_(funs(scale),vars= c("weight_bp", "weight_coda", "weight_hippo", "weight_pi", "weight_tm")) 

# calculate combined score - assumption here is that the scores are all equally
# important, one could implement additional weighting scheme here
merged_data_rescaled <- mutate(merged_data_rescaled, score=(1-((1-weight_bp)*(1-weight_coda)*(1-weight_hippo)*(1-weight_string)*(1-weight_tm))))
write.table(merged_data_rescaled, file="merged_data_rescaled.txt", row.names = F,sep="\t")


#--------------------------------------------------------------
# Section 5
#--------------------------------------------------------------
# Convert the data frame to an iGraph network
#--------------------------------------------------------------
library(igraph)
all_merged <- subset(merged_data_rescaled, select=c("id1","id2","weight_bp","weight_coda","weight_hippo","weight_string","weight_tm","score"))

# undirected network
network <- graph.data.frame(all_merged, directed=F)
summary(network)

is.simple(network)

# remove duplicate edges - we're taking mean of the edge weights
network <- simplify(network, remove.multiple = TRUE, edge.attr.comb = "mean")
summary(network)
is.simple(network)

#--------------------------------------------------------------
# filter network and delete weak edges under 0.5
#--------------------------------------------------------------
# NOTE: oversimplified approach where we reject all edges below
# a certain threshold. 
# alternative means - Split data into learn/validate/test train 
# a machine learning e.g. a random forest to discover the "true" interactions.
# a suitable package for that task would be e.g. Rattle
#--------------------------------------------------------------
network_0.5 = delete.edges(network, which(E(network)$score <0.5))

# remove disconnected nodes
network_0.5 = delete.vertices(network_0.5,which(degree(network_0.5)<1))
summary(network_0.5)

# we can plot here
# plot(network_0.5, edge.width=E(network)$score, vertex.size=0.25, vertex.label=NA, layout=layout.fruchterman.reingold)

# To get the weighted adjacency matrix from the network:
adj=get.adjacency(network_0.5,attr='score')

# to produce a data frame
adj=get.adjacency(network_0.5,attr='score',sparse=FALSE) 

# extract edge list from igraph with weights
network_list <- as.data.frame(cbind( get.edgelist(network) , E(network)$score ))

network_0.5_list <- as.data.frame(cbind( get.edgelist(network_0.5) , E(network_0.5)$score ))

# to round the weight values to 3 decimal places:
# network_0.5_list <- as.data.frame(cbind( get.edgelist(network_0.5) , round( E(network_0.5)$weight, 3 )))

# rename columns
network_list <- rename(network_list, c("V1" = "from", "V2" = "to", "V3" = "weight"))

network_0.5_list <- rename(network_0.5_list, c("V1" = "from", "V2" = "to", "V3" = "weight"))
# names(network_0.5_list)[names(network_0.5_list) == 'V1'] <- 'from'

# write graph
#write.graph(network,"network.txt",format="edgelist")
#write.graph(network_0.5,"network_0.5.txt",format="edgelist")

# write the edgelist data frames
# network_list - unfiltered net
write.table(network_list, file="network_list_2018.txt", row.names = F, quote = FALSE, sep="\t")
# network_0.5_list - thresholded net
write.table(network_0.5_list, file="network_0.5_list_2018.txt", row.names = F, quote = FALSE, sep="\t")

#--------------------------------------------------------------
# Section 6
#------------------------------------------------------------------------------------
# enrichment with ClusterProfiler - a package for enrichment and comparisons -
# we will be using Cytoscape with EnrichmentMap for functional integration
#------------------------------------------------------------------------------------
# quick STRING plot
#------------------------------------------------------------------------------------
hits = main_string_mapping$STRING_id
# plot the STRING network png  
string_db$plot_network(hits)

#------------------------------------------------------------------------------------
# iGraph quick processing on String network-only
#------------------------------------------------------------------------------------
# convert table to igraph object
network_string <- graph.data.frame(main_string_interactions, directed=F)
summary(network_string)

# If you wanted to explicitly modify edges, you can do it here like:
#E(network)$combined_score <- main_string_interactions$combined_score
# remove duplicate edges (no duplicates here)
#network <- simplify(network, remove.multiple = TRUE, edge.attr.comb = "mean")

# highest confidence network combined_score >= 900
# conditionally delete all edges below 900
network_0.9 = delete.edges(network_string, which(E(network_string)$combined_score <=900))
# remove disconnected nodes
network_0.9 = delete.vertices(network_0.9,which(degree(network_0.9)<1))
summary(network_0.9)
#plot(network_0.9)
#plot(network, edge.width=E(network)$combined_score/100, vertex.size=0.25, vertex.label=NA, layout=layout.fruchterman.reingold)

#--------------------------------------------------------------
# custom enrichment with ClusterProfiler
#--------------------------------------------------------------
library(clusterProfiler)
library(pathview)
library(org.Hs.eg.db)
#library(RDAVIDWebService)
library(DOSE)

# expression values:
data(geneList)
de <- names(geneList)[geneList > 1]

# extract entrez id's from the annotation frame 'results' created with BioMart earlier
list_Entrez <- as.list(results$entrezgene)

# enrich against the whole of KEGG
# for a custom background we can use the universe switch
# which is simply a list of entrez id's same as the list_Entrez for input genes
kk <- enrichKEGG(list_Entrez, organism="human", pvalueCutoff=0.05, pAdjustMethod="BH", qvalueCutoff=0.1)
head(summary(kk))
summary(kk)


# Extract networks from KEGG into iGraph
hsa03430 <- pathview(gene.data = as.character(list_Entrez), pathway.id = "hsa03430",species = "human", limit = list(gene = max(abs(geneList)),cpd = 1))

hsa03030 <- pathview(gene.data = as.character(list_Entrez), pathway.id = "hsa03030",species = "human", limit = list(gene = max(abs(geneList)),cpd = 1))

# repeat for gene ontology
kg <- enrichGO(list_Entrez, OrgDb = "org.Hs.eg.db", ont="BP", pvalueCutoff=0.05, pAdjustMethod="BH", qvalueCutoff=0.1, minGSSize = 5)
head(summary(kg))
summary(kg)

enrichMap(kk)
dotplot(kk)
barplot(kk, showCategory = 15)
dotplot(kg)
barplot(kg, showCategory = 15)

#write.table(kk, file="03. extraction of known interactions from STRING/kegg_on_hits.txt", row.names = F, quote = FALSE, sep="\t")
#write.table(kg, file="03. extraction of known interactions from STRING/go_on_hits.txt", row.names = F, quote = FALSE, sep="\t")


result <- summary(kk)
write.table(result, file="KEGG_enrichment_2017.txt", row.names = F, quote = FALSE, sep="\t")

# export entrez ID's for the hit list (will be used for filtering in Cytoscape)
lapply(list_Entrez, write, "entrez_list_hits_2017.txt", append=TRUE, ncolumns=1000)


#enrichMap is very useful to generate a meaningful graph of clusters of enriched pathways.
#Although we can use fixed=FALSE parameter and then nodes and edges can be moved around, 
# some user prefer using Cytoscape to further customize the graph.

#DOSE/clusterProfiler/ReactomePA support this feature indirectly.
# User can use:
info <- enrichMap(kk)
info <- enrichMap(kk, n=250, vertex.label.font = 1, fixed=FALSE)
#----- working up to here

# info is a list of two data.frames: edge_data and vertex_data
# we can now export them to files for use in Cytoscape
write.table(info$edge_data, file="edges.txt", row.names = F, quote = FALSE, sep="\t")
write.table(info$vertex_data, file="nodes.txt", row.names = F, quote = FALSE, sep="\t")

# Export files for Gene Ontology


#--------------------------------------------------------------
# Section 7
#--------------------------------------------------------------
# Transfer networks to Cytoscape using the REST service
# Note this is a bi-directional access to and from CS via the API
# go here for details: http://idekerlab.github.io/cyREST/
# contact me if you'd like help setting it up
#--------------------------------------------------------------
library(RJSONIO)
library(igraph)
library(httr)

# Basic settings
port.number = 1234
base.url = paste("http://localhost:", toString(port.number), "/v1", sep="")

print(base.url)

version.url = paste(base.url, "version", sep="/")
cytoscape.version = GET(version.url)
cy.version = fromJSON(rawToChar(cytoscape.version$content))
print(cy.version)
# load the utility file (needs to be in your working directory)
source("cytoscape_util.R")

# Convert to Cytoscape style JSON object
cygraph <- toCytoscape(network)

# Send the complete network to Cytoscape
network.url = paste(base.url, "networks", sep="/")
res <- POST(url=network.url, body=cygraph, encode="json")
# Extract SUID of the new network
network.suid = unname(fromJSON(rawToChar(res$content)))

# repeat sending for the filtered network
cygraph <- toCytoscape(network_0.5)
# Send to Cytoscape
network.url = paste(base.url, "networks", sep="/")
res <- POST(url=network.url, body=cygraph, encode="json")
# Extract SUID of the new network
network.suid = unname(fromJSON(rawToChar(res$content)))



#--------------------------------------------------------------
# create custom style
style.name = "R Style"

# Defaults
def.node.color <- list(
  visualProperty = "NODE_FILL_COLOR",
  value = "#00aabb"
)

def.node.border.width <- list(
  visualProperty = "NODE_BORDER_WIDTH",
  value = 10
)

def.node.size <- list(
  visualProperty = "NODE_SIZE",
  value = 25
)

def.edge.target.arrow <- list(
  visualProperty="EDGE_TARGET_ARROW_SHAPE",
  value="ARROW"
)

def.edge.width <- list(
  visualProperty="EDGE_WIDTH",
  value=3
)

defaults <- list(
  def.node.color,
  def.node.border.width,
  def.node.size,
  def.edge.target.arrow,
  def.edge.width)

# Visual Mappings
min.betweenness = min(V(graph1)$betweenness)
max.betweenness = max(V(graph1)$betweenness)

mappings = list()

point1 = list(
  value=min.betweenness,
  lesser= "20.0",
  equal="20.0",
  greater="20.0"
)

point2 = list(
  value=max.betweenness,
  lesser="100.0",
  equal="100.0",
  greater="100.0"
)

node.size.continuous.points = list(point1, point2)

node.size = list(
  mappingType="continuous",
  mappingColumn="betweenness",
  mappingColumnType="Double",
  visualProperty="NODE_SIZE",
  points = node.size.continuous.points
)

node.label = list(
  mappingType="passthrough",
  mappingColumn="name",
  mappingColumnType="String",
  visualProperty="NODE_LABEL"
)

mappings = list(node.size, node.label)

style <- list(title=style.name, defaults = defaults, mappings = mappings)
style.JSON <- toJSON(style)

style.url = paste(base.url, "styles", sep="/")
POST(url=style.url, body=style.JSON, encode = "json")

#--------------------------------------------------------------
# apply force-directed layout
apply.layout.url = paste(
  base.url,
  "apply/layouts/force-directed",
  toString(network.suid),
  sep="/"
)
#--------------------------------------------------------------
# apply custom layout
apply.style.url = paste(
  base.url,
  "apply/styles",
  style.name,
  toString(network.suid),
  sep="/"
)

#--------------------------------------------------------------
# load both
res <- GET(apply.layout.url)
res <- GET(apply.style.url)

# alternative connections with Cytoscape rserve, cyrface, cytoscapeRPC

#--------------------------------------------------------------