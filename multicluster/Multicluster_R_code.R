# Matt Rogon
# rogon@embl.de
# 18.05.2018
# ver. 0.3


library(dplyr)
library(STRINGdb)
library(reshape2)
#source("https://bioconductor.org/biocLite.R")
#biocLite()
#biocLite(c("clusterProfiler", "ReactomePA","org.Mm.eg.db", "pathview", "DOSE"))


setwd("/Users/rogon/Dropbox/2018_EBI/02. Friday/multicluster")
# load complete 5 cluster-dataset
hit_list <- read.delim("550prot_clusters.txt", stringsAsFactors=FALSE)

#--------------------------------------------------------------
# setup the STRINGdb environment
#--------------------------------------------------------------
species <- get_STRING_species(version="10", species_name=NULL)
string_db <- STRINGdb$new( version="10", species=10090, score_threshold=0, input_directory="" )

# map identifiers to string id's (automatic format detection) - notice that these are
# ENSEMBL protein ID's concatenated with species id
main_string_mapping = string_db$map(hit_list, "UniProt", removeUnmappedRows = TRUE )

# extract interactions for upregulated proteins
string_interactions <- string_db$get_interactions(main_string_mapping$STRING_id)

# backup
dataset_string <- string_interactions

# Downloaded a total of 8585 interactions for 535 proteins
# String scores
# 0.15 low
# 0.4 medium
# 0.7 high
# 0.9 very high

# we have 879 high scoring interactions
table(dataset_string$combined_score >= 700)

#plot_hits = main_string_mapping$STRING_id[1:100]
#plot the STRING network png
#string_db$plot_network(hit_list)

#------------------------------------------------------------------------------------
# reannotate ensembl to uniprot
#------------------------------------------------------------------------------------
# reannotate network from and to columns from ensembl to uniprot
main_string_mapping_annot <- dplyr::select(main_string_mapping, STRING_id, UniProt)

dataset_string <- left_join(dataset_string, main_string_mapping_annot, by = c("from" = "STRING_id"))
# rename
dataset_string <- plyr::rename(dataset_string, c("UniProt" = "from_uniprot"))

dataset_string <- left_join(dataset_string, main_string_mapping_annot, by = c("to" = "STRING_id"))
# rename
dataset_string <- plyr::rename(dataset_string, c("UniProt" = "to_uniprot"))

#------------------------------------------------------------------------------------
# trim species taxid from the STRING datasets
#------------------------------------------------------------------------------------
dataset_string$from <- gsub("10090.", "", dataset_string$from, fixed=TRUE)
dataset_string$to <- gsub("10090.", "", dataset_string$to, fixed=TRUE)

# output file
dataset_string_sorted <- dplyr::select(dataset_string, from_uniprot, to_uniprot, from, to, neighborhood, neighborhood_transferred, fusion, cooccurence, homology, coexpression, coexpression_transferred, experiments, experiments_transferred, database, database_transferred, textmining, textmining_transferred, combined_score)

write.table(dataset_string_sorted, file="output/network_list.txt", row.names = F, quote = FALSE, sep="\t")

#--------------------------------------------------------------
# create an annotation file for mapping ensembl (STRING) id to uniprot
#--------------------------------------------------------------
main_string_mapping$STRING_id <- gsub("10090.", "", main_string_mapping$STRING_id, fixed=TRUE)
# select columns and write the annotation file
main_string_mapping_select <- dplyr::select(main_string_mapping, STRING_id, UniProt, cluster)
# write file
write.table(main_string_mapping_select, file="output/network_list_annotation.txt", row.names = F, quote = FALSE, sep="\t")

#--------------------------------------------------------------
# Section x
#------------------------------------------------------------------------------------
# Annotation with bitr in ClusterProfiler for enrichment check
#------------------------------------------------------------------------------------
library(ReactomePA)
library(clusterProfiler)
library(pathview)
library(DOSE)
library(org.Mm.eg.db)
keytypes(org.Mm.eg.db)

entrez_map <- bitr(main_string_mapping_select$UniProt, fromType="UNIPROT", toType=c("ENTREZID", "SYMBOL"), OrgDb="org.Mm.eg.db", drop= FALSE)

write.table(entrez_map, file="output/entrez_map.txt", row.names = F, quote = FALSE, sep="\t")


#--------------------------------------------------------------
# custom enrichment with ClusterProfiler
#--------------------------------------------------------------
# expression values:
#data(geneList)
#de <- names(geneList)[geneList > 1]

# to do enrichment on a 'per cluster' basis I need the cluster column from the data frame: main_string_mapping_select
input_for_enrichment <- merge(main_string_mapping_select,entrez_map, by.x = "UniProt", by.y = "UNIPROT", all = TRUE)

# extract entrez id's from the annotation frame 'results' created earlier
# for each cluster separately

#c1 <- subset(input_for_enrichment, cluster==1)
#c1_s <- dplyr::select(c1, ENTREZID)
#c1_s <- unique(c1_s)
#c1_list <- as.list(c1_s$ENTREZID)

#same in 2 lines
c1 <- dplyr::select(subset(input_for_enrichment, cluster ==1), ENTREZID)
c1_list <- as.list(c1$ENTREZID)

c2 <- dplyr::select(subset(input_for_enrichment, cluster ==2), ENTREZID)
c2_list <- as.list(c2$ENTREZID)

c3 <- dplyr::select(subset(input_for_enrichment, cluster ==3), ENTREZID)
c3_list <- as.list(c3$ENTREZID)

c4 <- dplyr::select(subset(input_for_enrichment, cluster ==4), ENTREZID)
c4_list <- as.list(c4$ENTREZID)

c5 <- dplyr::select(subset(input_for_enrichment, cluster ==5), ENTREZID)
c5_list <- as.list(c5$ENTREZID)


hit_list_Entrez_flat <- unlist(hit_list_Entrez)
hit_list_Entrez2_flat <- unlist(hit_list_Entrez2)


#---------------------------------------------------------------------------------------
# Reactome enrichment
#---------------------------------------------------------------------------------------
library(ReactomePA)
c1_reactome <- ReactomePA::enrichPathway(gene=c1_list, organism="mouse", pvalueCutoff=0.05, pAdjustMethod="BH", qvalueCutoff=0.1)
head(summary(c1_reactome))

# enrich against the whole of KEGG
# for a custom background we can use the universe switch
# which is simply a list of entrez id's same as the list_Entrez for input genes
# organisms: http://www.genome.jp/kegg/catalog/org_list.html

kegg_1 <- enrichKEGG(c1_list, organism="mouse", pvalueCutoff=0.05, pAdjustMethod="BH", qvalueCutoff=0.1)
kegg_2 <- enrichKEGG(c2_list, organism="mouse", pvalueCutoff=0.05, pAdjustMethod="BH", qvalueCutoff=0.1)
kegg_3 <- enrichKEGG(c3_list, organism="mouse", pvalueCutoff=0.05, pAdjustMethod="BH", qvalueCutoff=0.1)
kegg_4 <- enrichKEGG(c4_list, organism="mouse", pvalueCutoff=0.05, pAdjustMethod="BH", qvalueCutoff=0.1)
kegg_5 <- enrichKEGG(c5_list, organism="mouse", pvalueCutoff=0.05, pAdjustMethod="BH", qvalueCutoff=0.1)

head(summary(kegg_2))
summary(kegg_4)


#----
# Cluster-wise analysis - see the separate directory for sample files and code - 
# clusters within this sample will not produce an overlap
#----

input_all = list()
input_all$X1 <- c1_list
input_all$X2 <- c2_list
input_all$X3 <- c3_list
input_all$X4 <- c4_list
input_all$X5 <- c5_list
summary(input_all)

# Gene Ontology comparison
res_go <- compareCluster(input_all, fun="enrichGO", OrgDb="org.Hs.eg.db", pvalueCutoff=0.1) 
dotplot(res_go, title="GO Enrichment Comparison")
res_reactome <- compareCluster(input_all, fun="enrichPathway", organism='human', pvalueCutoff=0.01, pAdjustMethod="BH", qvalueCutoff=0.1)
dotplot(res_reactome, title="Reactome Enrichment Comparison")
